import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RandomList {
    private static Random rand = new Random();

    public static List<Integer> randomUsingGenerate(int count,int max){
        List<Integer> list = Stream.generate(() -> (rand.nextInt(max)))
                .limit(count)
                .collect(Collectors.toList());
        return list;
    }

    public static List<Integer> randomUsingIterate(int start,int max, int count){
        List<Integer> list = Stream.iterate(start,(n) -> (rand.nextInt(max)))
                .limit(count)
                .collect(Collectors.toList());
        return list;
    }
}
