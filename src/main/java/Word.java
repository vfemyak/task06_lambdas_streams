import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Word {
    public static int uniqueCount (List<String> words){
        return (int)words.stream()
                .flatMap(s -> Stream.of(s.split(" ")))
                .distinct()
                .count();
    }
    public static List<String> uniqueSort (List<String> words){
        return words.stream()
                .flatMap(s -> Stream.of(s.split(" ")))
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }
    public static void charCount (String words){
        List<Character> textInChar = words.chars().mapToObj(e->(char)e).collect(Collectors.toList());
        Map<Character, Integer> countChar = textInChar.stream().collect(HashMap::new, (m, c) -> {
            m.put(c, m.containsKey(c) ? (m.get(c) + 1) : 1);
        }, HashMap::putAll);

        countChar.forEach( (k, v) -> System.out.println(k + " -> " + v));
    }
//    public static void wordCount (List<String> words){
//        Map<String, Integer> countWord = words.stream().collect(HashMap::new,(s, s2) -> {
//            s.put
//        });
//
//        countWord.forEach( (k, v) -> System.out.println(k + " -> " + v));
//    }
}
