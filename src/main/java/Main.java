import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {
    private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    public static void main(String[] args) throws IOException {

        /*task1_v1.0*/
//        IntSummaryStatistics stat = IntStream.of(1,17,5).summaryStatistics();
//        System.out.println(stat);


        /*task1_v2.0*/
//        Maxable lambda_max = (a,b,c) ->
//                ((a>=b && a>=c) ? a : (b>=a && b>=c) ? b : c);
//        Averageble lambda_avg = (a,b,c) ->
//                ((double)(a+b+c)/3);
//
//        System.out.println("Enter three numbers: ");
//        System.out.print("a = ");
//        int a = Integer.parseInt(br.readLine());
//        System.out.print("b = ");
//        int b = Integer.parseInt(br.readLine());
//        System.out.print("c = ");
//        int c = Integer.parseInt(br.readLine());
//
//        System.out.println("\nMax value is " + lambda_max.max(a,b,c));
//        System.out.println("Average value is " + lambda_avg.avarage(a,b,c));


        /*task2*/
//        Command lambda_cmd = (string) -> {System.out.println
//                ("This is lambda function. Your string: " + string );};  //lambda function
//        lambda_cmd.print("Hello!");
//
//        Command anon_cmd = new Command() {
//            @Override
//            public void print(String str) {
//                System.out.println("This is anonymous class. Your string: " + str );
//            }
//        };
//        anon_cmd.print("Hi!");
//
//        Command ref_cmd = lambda_cmd::print;
//        ref_cmd.print("This is anonymous class. Your string: ");


        /*task3*/
//        List<Integer> list = RandomList.randomUsingGenerate(10,50);
//        System.out.println("Using Generate: " + list);
////        List<Integer> list = RandomList.randomUsingIterate(10,100,10);
////        System.out.println("Using Iterate: " + RandomList.randomUsingIterate(10,100,10));
//
//        IntSummaryStatistics stat = list.stream().mapToInt(Integer::byteValue).summaryStatistics();
//        System.out.println(stat);
//
//        System.out.println("Sum of list elements = " + list.stream()
//                .reduce(0,Integer::sum));   //using both sum() and reduce()
//
//        System.out.println(list.stream()
//                .filter(integer -> integer >= stat.getAverage())
//                .count());  //counting elements which are bigger than average value


        /*task4*/
//        List list = br.lines().filter(String::isEmpty).collect(Collectors.toList());
//        System.out.println(list);

        List<String> lines = new ArrayList<String>();
        String s;
        while (true){
            s = br.readLine();
            if(s.isEmpty())
                break;
            lines.add(s);
        }

        System.out.println("Number of unique word = " + Word.uniqueCount(lines));
        System.out.println("Sorted unique words: " + Word.uniqueSort(lines));

        System.out.println("Number of each symbols: ");
        String concat = String.join(" ",lines).replaceAll("\\s", "").toLowerCase();
        Word.charCount(concat);

//        System.out.println("Number of each words: ");
//        Word.wordCount(lines);

    }
}
